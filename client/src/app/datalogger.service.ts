import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { DataLogger } from './datalogger';

import { MessageService } from './message.service';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DataloggerService {
  
  private dataloggersUrl = 'api/dataloggers';

  getDataloggers(): Observable<DataLogger[]>{
    this.messageService.add('DataloggerService: fetched Dataloggers');
    return this.http.get<DataLogger[]>(this.dataloggersUrl)
      .pipe(
        tap(_ => this.log('fetched dataloggers')),
        catchError(this.handleError<DataLogger[]>('getDataloggers', []))
      );
  }
  
  getDatalogger(id: string): Observable<DataLogger> {
    const url = `${this.dataloggersUrl}/${id}`;
    return this.http.get<DataLogger>(url).pipe(
      tap(_ => this.log(`fetched datalogger; id=${id}`)),
      catchError(this.handleError<DataLogger>(`getDatalogger id=${id}`))
    );
  }
  
  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }

  private log(message: string) {
    this.messageService.add(`DataloggerService: ${message}`);
  }
  
    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
}
}
