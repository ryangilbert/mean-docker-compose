import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataloggersComponent } from './dataloggers.component';

describe('DataloggersComponent', () => {
  let component: DataloggersComponent;
  let fixture: ComponentFixture<DataloggersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataloggersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataloggersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
