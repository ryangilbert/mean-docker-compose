import { Component, OnInit } from '@angular/core';

import { DataloggerService } from '../datalogger.service';

import { DataLogger } from '../datalogger';

@Component({
  selector: 'app-dataloggers',
  templateUrl: './dataloggers.component.html',
  styleUrls: ['./dataloggers.component.css']
})
export class DataloggersComponent implements OnInit {

  dataloggers: DataLogger[];
  
  constructor(private dataloggerService: DataloggerService) {}

  getDataloggers(): void {
    this.dataloggerService.getDataloggers()
      .subscribe(dataloggers => this.dataloggers = dataloggers);
  }
  
  ngOnInit() {
    this.getDataloggers();
  }

}
