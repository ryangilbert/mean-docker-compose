import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'; 

import { DataloggerService } from '../datalogger.service';

import { DataLogger } from '../datalogger';

@Component({
  selector: 'app-datalogger-detail',
  templateUrl: './datalogger-detail.component.html',
  styleUrls: ['./datalogger-detail.component.css']
})
export class DataloggerDetailComponent implements OnInit {

  @Input() datalogger: DataLogger;
  
  constructor(
    private route: ActivatedRoute,
    private dataloggerService: DataloggerService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getDatalogger();
  }
  
  getDatalogger(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.dataloggerService.getDatalogger(id)
      .subscribe(datalogger => this.datalogger = datalogger);
  }
  
  goBack(): void {
    this.location.back();
  }

}
