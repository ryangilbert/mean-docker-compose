import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataloggerDetailComponent } from './datalogger-detail.component';

describe('DataloggerDetailComponent', () => {
  let component: DataloggerDetailComponent;
  let fixture: ComponentFixture<DataloggerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataloggerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataloggerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
