import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataloggersComponent } from './dataloggers/dataloggers.component';
import { DataloggerDetailComponent } from './datalogger-detail/datalogger-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/dataloggers', pathMatch: 'full' },
  { path: 'dataloggers', component: DataloggersComponent },
  { path: 'detail/:id', component: DataloggerDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
